fn main() {
    let x = 5;

    let y = {
        let x = 3;
        x + 1
    };

    another_function(x, y);

    let x = seven();

    message('x', x);

    let x = plus_one(x);

    message('x', x);
}

fn message(name: char, x: i32) {
    println!("The value of {} is: {}", name, x);
}

fn another_function(x: i32, y: i32) {
    message('x', x);
    message('y', y);
}

fn seven() -> i32 {
    7
}

fn plus_one(x: i32) -> i32 {
    x + 1
}