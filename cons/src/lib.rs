use std::fmt::Formatter;
use std::fmt::Error;
use std::fmt::Display;
use List::{Cons, Nil};

#[derive(Debug)]
pub enum List {
    Cons(i32, Box<List>),
    Nil,
}

impl List {
    pub fn head(&self) -> Option<i32> {
        match self {
            Nil => None,
            Cons(h, _) => Some(*h),
        }
    }

    pub fn tail(&self) -> List {
        match self {
            Nil => Nil,
            Cons(_, t) => {
                if t.head() == None {
                    Nil
                } else {
                    Cons(t.head().unwrap(), Box::new(t.tail()))
                }
            }
        }
    }
}

impl Display for List {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        match self {
            Nil => write!(f, "()"),
            Cons(h, t) => write!(f, "({}, {})", h, t)
        }
    }
}
